package org.example.storage;

import org.example.shop.Item;
import org.example.shop.StandardItem;
import org.example.storage.ItemStock;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {

    @Test
    public void testConstructor() {
        Item item = new StandardItem(1, "Test Item", 10.0f, "Test Category", 5);
        ItemStock itemStock = new ItemStock(item);
        Assertions.assertEquals(item, itemStock.getItem());
        Assertions.assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "5, 3, 8",
            "0, 5, 5",
            "-5, 10, 5"
    })
    void testIncreaseItemCount(int initialCount, int increaseAmount, int expectedCount) {
        Item item = new StandardItem(1, "Test Item", 10.0f, "Test Category", 5);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(initialCount);
        itemStock.IncreaseItemCount(increaseAmount);
        Assertions.assertEquals(expectedCount, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({
            "10, 3, 7",
            "5, 5, 0",
            "0, 10, -10"
    })
    void testDecreaseItemCount(int initialCount, int decreaseAmount, int expectedCount) {
        Item item = new StandardItem(1, "Test Item", 10.0f, "Test Category", 5);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(initialCount);
        itemStock.decreaseItemCount(decreaseAmount);
        Assertions.assertEquals(expectedCount, itemStock.getCount());
    }
}
