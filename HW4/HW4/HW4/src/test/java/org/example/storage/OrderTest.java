package org.example.storage;

import org.example.shop.Order;
import org.example.shop.ShoppingCart;
import org.example.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testConstructorWithState() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 10.0f, "Test Category", 5));
        Order order = new Order(cart, "John Doe", "123 Main St", 1);
        assertEquals("John Doe", order.getCustomerName());
        assertEquals("123 Main St", order.getCustomerAddress());
        assertEquals(1, order.getState());
        assertEquals(1, order.getItems().size());
    }

    @Test
    void testConstructorWithoutState() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 10.0f, "Test Category", 5));
        Order order = new Order(cart, "John Doe", "123 Main St");
        assertEquals("John Doe", order.getCustomerName());
        assertEquals("123 Main St", order.getCustomerAddress());
        assertEquals(0, order.getState());
        assertEquals(1, order.getItems().size());
    }

    // TODO add test for NPE when cart is NULL

}
