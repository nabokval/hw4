package org.example.storage;


import org.example.shop.StandardItem;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
class StandardItemTest {

    @Test
    void testConstructor() {
        StandardItem item = new StandardItem(1, "Test Item", 10.0f, "Test Category", 5);
        assertEquals(1, item.getID());
        assertEquals("Test Item", item.getName());
        assertEquals(10.0f, item.getPrice());
        assertEquals("Test Category", item.getCategory());
        assertEquals(5, item.getLoyaltyPoints());
    }

    @Test
    void testCopy() {
        StandardItem originalItem = new StandardItem(1, "Test Item", 10.0f, "Test Category", 5);
        StandardItem copiedItem = originalItem.copy();
        assertEquals(originalItem, copiedItem);
    }

    @ParameterizedTest
    @CsvSource({
            "1, Test Item, 10.0, Test Category, 5, 1, Test Item, 10.0, Test Category, 5, true",
            "1, Test Item, 10.0, Test Category, 5, 2, Test Item, 10.0, Test Category, 5, false",
            "1, Test Item, 10.0, Test Category, 5, 1, Different Item, 10.0, Test Category, 5, false",
            "1, Test Item, 10.0, Test Category, 5, 1, Test Item, 20.0, Test Category, 5, false",
            "1, Test Item, 10.0, Test Category, 5, 1, Test Item, 10.0, Different Category, 5, false",
            "1, Test Item, 10.0, Test Category, 5, 1, Test Item, 10.0, Test Category, 10, false"
    })
    void testEquals(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                    int id2, String name2, float price2, String category2, int loyaltyPoints2,
                    boolean expectedResult) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        assertEquals(expectedResult, item1.equals(item2));
    }
}
