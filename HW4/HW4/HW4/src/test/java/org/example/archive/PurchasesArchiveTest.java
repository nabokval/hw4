package org.example.archive;


import org.example.shop.Item;
import org.example.shop.ShoppingCart;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.example.shop.Order;

class PurchasesArchiveTest {

    @Test
    void testPrintItemPurchaseStatistics() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Prepare
        ItemPurchaseArchiveEntry mockEntry1 = Mockito.mock(ItemPurchaseArchiveEntry.class);
        ItemPurchaseArchiveEntry mockEntry2 = Mockito.mock(ItemPurchaseArchiveEntry.class);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
        itemPurchaseArchive.put(1, mockEntry1);
        itemPurchaseArchive.put(2, mockEntry2);
        // create purchase archive with the mocked item purchase archive
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchive, new ArrayList<>());
        
        // act
        purchasesArchive.printItemPurchaseStatistics();

        // verify
        String expectedOutput = "ITEM PURCHASE STATISTICS:" + System.lineSeparator() + ":TODO:";
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    void testGetHowManyTimesHasBeenItemSold() {
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        ItemPurchaseArchiveEntry mockEntry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        when(mockEntry.getCountHowManyTimesHasBeenSold()).thenReturn(5);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
        itemPurchaseArchive.put(1, mockEntry);
        purchasesArchive.putOrderToPurchasesArchive(new Order(new ShoppingCart() , "", ""));
        int result = purchasesArchive.getHowManyTimesHasBeenItemSold(new Item(1, "", 5, ""));
        assertEquals(0, result);
    }

    @Test
    void testPutOrderToPurchasesArchive() {
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        Order mockOrder = Mockito.mock(Order.class);
        Item mockItem = Mockito.mock(Item.class);
        when(mockItem.getID()).thenReturn(1);
        when(mockOrder.getItems()).thenReturn(new ArrayList<Item>(){{add(mockItem);}});
        purchasesArchive.putOrderToPurchasesArchive(mockOrder);
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(mockItem));
    }

    @Test
    void testItemPurchaseArchiveEntryConstructor() {
        // Volání konstruktoru
        Item item = new Item(1, "TestItem", 10, "Test Category");
        ItemPurchaseArchiveEntry entry = new ItemPurchaseArchiveEntry(item);
        // Ověření, že konstruktor byl volán s daným argumentem
        assertEquals(item, entry.getRefItem());
    }
}
