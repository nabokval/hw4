package org.example.shop;

import org.example.shop.*;
import org.example.storage.*;
import org.example.archive.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class EShopControllerTest {

    @Test
    public void testPurchaseShoppingCart() throws NoItemInStorage {
        // Inicializace EShopController
        EShopController.startEShop();

        // Vytvoření nákupního košíku
        ShoppingCart cart = new ShoppingCart();
        Item item = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 1);
        cart.addItem(item);

        // Vložení položky do skladu
        EShopController.storage.insertItems(item, 1);

        // Nákup košíku
        EShopController.purchaseShoppingCart(cart, "Libuse Novakova", "Kosmonautu 25, Praha 8");

        // Ověření, že košík byl správně zakoupen a položka byla odebrána ze skladu
        assertEquals(0, EShopController.storage.getItemCount(1)); // Očekává se, že položka byla odebrána ze skladu
        assertEquals(1, EShopController.archive.getHowManyTimesHasBeenItemSold(item)); // Očekává se, že položka byla správně archivována
    }

    @Test
    public void testPurchaseEmptyShoppingCart() {
        // Inicializace EShopController
        EShopController.startEShop();

        // Vytvoření prázdného nákupního košíku
        ShoppingCart emptyCart = new ShoppingCart();
        // Ověření, že při pokusu o nákup prázdného košíku je vyhozena výjimka NoItemInStorage
        NoItemInStorage exception = assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(emptyCart, "Jarmila Novakova", "Spojovaci 23, Praha 3");
        });
        // TODO add validation for the correct exception message
    }
}
